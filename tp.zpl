set materias := {read "materias.txt" as "<1s>" comment "#"};
set horarios := {read "horarios.txt" as "<1s>" comment "#"};
param cargaHoraria[materias] := read "cargaHoraria.txt" as "<1s> 2n" comment "#";

set correlativas[materias] := <"IP"> {},
	<"IM"> {},
	<"TLED"> {},
	<"P1"> {"IP"},
	<"LTN"> {"IM"},
	<"OC1"> {"IP"},
	<"P2"> {"P1","IM"},
	<"AL"> {"IM"},
	<"SOR1"> {"OC1","P1"},
	<"P3"> {"P2"},
	<"CP"> {"IM","AL"},
	<"PSEC"> {},
	<"BD1"> {"LTN","P2","OC1"},
	<"MD"> {"LTN","CP","AL"},
	<"EVS"> {"LTN","P3"},
	<"TC"> {"P3","MD","OC1"},
	<"IS1"> {"P3"},
	<"PE"> {"CP","MD"},
	<"PP1"> {"PSEC","TLED","BD1","IS1","EVS"},
	<"IS2"> {"IS1","EVS"},
	<"OC2"> {"OC1"},
	<"PP2"> {"PP1"},
	<"BD2"> {"BD1","P3"},
	<"SOR2"> {"SOR1"},
	<"PPS"> {"PP2","BD2"},
	<"MO"> {"PE"},
	<"IS"> {"IS1"},
	<"TTL1"> {"PP2","BD2"},
	<"TTL2"> {"TTL1"},
	<"GP"> {"IS2","PP1"},
	<"LI"> {"PP1"}, 
	<"TU"> {},
	<"IL1"> {},
	<"IL2"> {"IL1"},
	<"IL3"> {"IL2"};

# param MAXCUATRIMESTRES := card(materias);
param MAXCUATRIMESTRES := 11;

set cuatrimestres := {1..MAXCUATRIMESTRES};

var x[cuatrimestres] binary;

var y[materias*cuatrimestres] binary;

var z[materias*cuatrimestres*horarios] binary;

minimize tiempo: sum <c> in cuatrimestres: x[c];

# Una materia debe aparecer en 1 solo cuatrimestre del itinerario.
subto r1:
    forall <m> in materias:
		sum <c> in cuatrimestres: y[m,c] == 1;

# Las materia m debe ser cursada en un cuatrimestre posterior al de 
# cualquiera de sus materias correlativas n.
subto r2a:
	forall <m> in materias:
		forall <n> in correlativas[m]:
			sum <c> in cuatrimestres: c * y[m,c] >= sum <c> in cuatrimestres: c * y[n,c] + 1;

# Para elegir un materia, debo haber asignado las correlatividades.
# subto r2b:
# 	forall <c> in cuatrimestres:
#  		forall <m> in materias:
#  			forall <j> in correlativas[m]:
#  				y[m,c] <= (sum <k> in cuatrimestres with k<c: y[j,k]);

# Una materia no puede superar la cantidad máxima de cuatrimestres.
subto r3:
	forall <c> in cuatrimestres:
		sum <m> in materias: y[m,c] <= x[c] * MAXCUATRIMESTRES;

# Se anota en cuatrimestres seguidos
subto r4:
    forall <c> in cuatrimestres with c != MAXCUATRIMESTRES:
        x[c] >= x[c+1];

# La carga horaria de las materias cursadas en un cuatrimestre 
# # no puede ser mayor a la cantidad de horas semanales.
# subto r5:
# 	forall <c> in cuatrimestres:
# 		sum <m> in materias: cargaHoraria[m]  * y[m,c] <= card(horarios) * 2;

# Se copian los valores de la matriz materia*cuatrimestre a la matriz materia*cuatrimestre*horario.
subto r6:
	forall <m> in materias:
		forall <c> in cuatrimestres:
			forall <h> in horarios:
				z[m,c,h] <= y[m,c];

# A la materia m se le elije exactamente la misma cantidad de horas que indica su carga horaria.
subto r7:
	forall <m> in materias:
		forall <c> in cuatrimestres:
			sum <h> in horarios: z[m,c,h] >= (cargaHoraria[m]/2) * y[m,c];

# Aquellas materias que se cursan en el mismo cuatrimestre no pueden dictarse en el mismo horario.
subto r8:
	forall <m> in materias:
		forall <n> in materias with m!=n:
			forall <c> in cuatrimestres:
				forall <h> in horarios:
					z[m,c,h] + z[n,c,h] <= 1;